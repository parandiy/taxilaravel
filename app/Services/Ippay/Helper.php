<?php

/**
 * IPpay Gateway
 */

namespace App\Services\Ippay;

use Omnipay\Common\CreditCard;

class Helper
{
    public static function getToken($creditCardData)
    {
        $token = '';

        $gateway = new Gateway;
        $gateway->setTerminalId(config('taxi.ippay.terminal'));
        $gateway->setTestMode(config('taxi.ippay.test_mode'));

        $card       = new CreditCard($creditCardData);
        $ccResponse = $gateway->createCard([
            'card' => $card
        ])->send();

        $message = $ccResponse->getMessage();

        $response = new \SimpleXMLElement($message);
        if ($ccResponse->isSuccessful()) {
            $token = $response->Token;
        } else {
            $error = htmlentities($response->getMessage());
            throw new \Exception($error);
        }

        return $token;
    }

    public static function purchase($token, $amount)
    {
        $gateway = new Gateway;
        $gateway->setTerminalId(config('taxi.ippay.terminal'));
        $gateway->setTestMode(config('taxi.ippay.test_mode'));

        $response = $gateway->purchase([
            'transaction_type' => 'SALE',
            'amount'           => $amount,
            'token'            => $token, //previously saved
        ])->send();

        if ($response->isSuccessful()) {
            $message  = $response->getMessage();
            $response = new \SimpleXMLElement($message);

            return (array)$response;
        } else {
            $error = htmlentities($response->getMessage());
            throw new \Exception($error);
        }

        return false;
    }
}
