<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job
    extends Model
{
    protected $fillable = [
        'order_date',
        'from',
        'to',
        'passengers',
        'passengers_fee',
        'baggage_seats',
        'baggage_fee',
        'distance',
        'travel_time',
        'cab_number',
        'amount_payment',
        'amount_tip',
        'amount_total',
        'payment_type',
        'operation_cost',
        'comments',
        'viewed',
        'advance_booking',
        'send_notify',
        'user_timezone',
    ];

    protected $dates = [
        'order_date',
    ];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class);
    }

    public function transaction()
    {
        return $this->hasOne(\App\Models\Transaction::class);
    }

    public function scopeViewed($query, $isViewed)
    {
        return $query->where('viewed', $isViewed);
    }


    public function toXmlData()
    {
        $data = [
            'JobInformation'      => [
                'JobNumber'      => $this->id,
                'OrderTime'      => $this->created_at->timezone($this->user_timezone)->format('Y-m-d H:i:s'),
                'PickupTime'     => $this->order_date,
                'From'           => $this->from,
                'To'             => $this->to,
                'Passengers'     => $this->passengers,
                'PassengersFee'  => $this->passengers_fee,
                'BaggageSeats'   => $this->baggage_seats,
                'BaggageFee'     => $this->baggage_fee,
                'Distance'       => $this->distance,
                'TravelTime'     => $this->travel_time,
                'CabNumber'      => $this->cab_number,
                'PaymentAmount'  => $this->amount_total,
                'AmountTip'      => $this->amount_tip,
                'OperationCost'  => $this->operation_cost,
                'Comments'       => $this->comments,
                'AdvanceBooking' => $this->advance_booking,
                'PaymentType'    => $this->payment_type,
            ],
            'CustomerInformation' => [
                'CustomerName'    => $this->customer->first_name . ' ' . $this->customer->last_name,
                'CustomerAddress' => $this->customer->address . ', ' . $this->customer->city . ', ' . ((!empty($this->customer->state)) ? $this->customer->state->name : '') . ', ' . $this->customer->zip_code,
                'CustomerEmail'   => $this->customer->email,
                'CustomerPhone'   => $this->customer->phone,
            ]
        ];

        if (!empty($this->transaction)) {
            $data['PaymentInformation'] = [
                'CardHolderName' => $this->transaction->card_holder_name,
                'CardNumber'     => 'xxxx' . substr($this->transaction->card_number, -4),
                'ApprovalCode'   => $this->transaction->approval,
                'TransactionID'  => $this->transaction->transaction_id
            ];
        }

        return $data;
    }
}
