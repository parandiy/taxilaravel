<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction
    extends Model
{
    protected $fillable = [
        'card_holder_name',
        'card_number',
        'action_code',
        'approval',
        'response_text',
        'verification_result',
        'transaction_id',
    ];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class);
    }

    public function job()
    {
        return $this->belongsTo(\App\Models\Job::class);
    }
}
