<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer
    extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
    ];

    public function jobs()
    {
        return $this->hasMany(\App\Models\Job::class);
    }

    public function state()
    {
        return $this->belongsTo(\App\Models\State::class);
    }
}
