<?php

namespace App\Actions;

use App\Mail\CustomerNotification;
use App\Mail\OperatorNotification;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
use BabDev\Twilio\Facades\TwilioClient;
use Log;
use TeamPickr\DistanceMatrix\DistanceMatrix;
use TeamPickr\DistanceMatrix\Licenses\StandardLicense;

class CalculationActions
{
    public static function calculate($origin, $destination, $passengers = 1)
    {
        $license = new StandardLicense(config('google.key'));

        $response = DistanceMatrix::license($license)->addOrigin($origin)->addDestination($destination)->request();

        $rows = $response->rows();
        if (isset($rows[0])) {
            $elements = $rows[0]->elements();
            if (isset($elements[0])) {
                $element = $elements[0];

                $distance = $element->distance();
                $duration = $element->duration();

                $milesKm  = 1609;
                $settings = Setting::pluck('value', 'key')->toArray();

                $miles = round($distance / $milesKm, 2);

                $sumPerTime         = ($duration / $settings['pay_per_seconds']) * $settings['pay_per_seconds_sum'];
                $sumPerMiles        = $miles * $settings['price_per_mile'];
                $sumExtraPassengers = (($passengers - 1) * $settings['price_extra_passenger']) + (float)$settings['passenger_fee'];

                $totalSum = round($sumPerTime + $sumPerMiles + $sumExtraPassengers, 2);

                return [
                    'distance'             => $miles,
                    'duration'             => $duration,
                    'sum'                  => $totalSum,
                    'sum_extra_passengers' => $sumExtraPassengers
                ];
            }
        }

        return false;
    }
}
