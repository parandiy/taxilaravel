<?php

namespace App\Actions;

use App\Mail\CustomerNotification;
use App\Mail\OperatorNotification;
use Illuminate\Support\Facades\Mail;
use BabDev\Twilio\Facades\TwilioClient;
use Log;

class OrderNotificationActions
{
    /**
     * @param $customer
     * @param $job
     * @param $transaction
     *
     * @return bool
     */
    public static function sendSmsToCustomer($customer, $job, $transaction)
    {
        if (strpos($customer->phone, '+') === false) {
            $customer->phone = '+1' . $customer->phone;
        }

        $text = 'Your taxi order for $' . $job->amount_total . ' has been processed successfully.';
        $text .= ' Pick-up address: ' . $job->from . '.';
        if (!empty($job->order_date)) {
            $text .= 'Pick-up time: ' . $job->order_date->format('M, d Y h:i A') . '.';
        }
        $text .= ' Drop-off address: ' . $job->to . '.';
        $text .= ' Amount: $' . $job->amount_total . '.';
        if ($transaction) {
            $text .= ' Card Used: xxx' . substr($transaction->card_number, -4) . '.';
            $text .= ' Please show this code ' . $transaction->approval . ' to the cab driver when the taxi arrives.';
        }
        $text .= ' Thank you.';
        $text .= ' Associate Taxi of Rochester (585)232-3232.';

        try {
            TwilioClient::connection()->message($customer->phone, $text);

            return true;
        } catch (\Exception $e) {
            Log::error('Message not sent to number: ' . $customer->phone);
            Log::error($e->getMessage());

            return false;
        }
    }

    /**
     * @param $customer
     * @param $job
     * @param $transaction
     *
     * @return bool
     */
    public static function sendSmsToOperator($customer, $job, $transaction)
    {
        $text = 'ASSOCIATE TAXI ';
        $text . '232-3232. ';
        $text . 'Job#: ' . $job->id . '.';
        if ($transaction) {
            $text . ' Approval Code: ' . $transaction->approval;
        }
        $text . ' Pickup: ' . $job->from . '.';
        if (!empty($job->order_date)) {
            $text .= ' Pick-up time: ' . $job->order_date->format('M, d Y h:i A') . '.';
        }
        $text . ' Destination: ' . $job->to . '.';
        $text . ' Customer Name: ' . $customer->first_name . ' ' . $customer->last_name . '.';
        if ($transaction) {
            $text . ' Card Holder Name: ' . $transaction->card_holder_name . '.';
            $text . ' Credit Card: xxx' . substr($transaction->card_number, -4) . '.';
        }

        try {
            TwilioClient::connection()->message(config('taxi.operator_phone'), $text);

            return true;
        } catch (\Exception $e) {
            Log::error('Message not sent to number: ' . config('taxi.operator_email'));
            Log::error($e->getMessage());

            return false;
        }
    }
}
