<?php

namespace App\Actions;

use App\Http\Requests\PaymentRequest;
use App\Models\Customer;
use App\Models\State;

class OrderSaveActions
{
    public static function saveCustomer(PaymentRequest $request)
    {
        $names = explode(' ', ($request->input('payment_method', 'card') == 'card') ? $request->input('payer_name') : $request->input('user_name'));
        if (empty($names[1])) {
            $names[1] = '';
        }

        $customer = Customer::where('email', $request->input('user_email'))->first();
        if (!$customer) {
            $customer = new Customer([
                'first_name' => $names[0],
                'last_name'  => $names[1],
                'email'      => $request->input('user_email'),
                'phone'      => $request->input('user_phone'),
                'address'    => $request->input('user_address'),
                'city'       => $request->input('user_city'),
                'zip_code'   => $request->input('user_zipcode'),
            ]);

            $state = State::find($request->input('state_id', 0));
            if ($state) {
                $customer->state()->associate($state);
            }

            $customer->save();
        } else {
            $customer->update([
                'first_name' => $names[0],
                'last_name'  => $names[1],
                'phone'      => $request->input('user_phone'),
                'address'    => $request->input('user_address'),
                'city'       => $request->input('user_city'),
                'zip_code'   => $request->input('user_zipcode'),
            ]);
        }

        return $customer;
    }

    public static function saveJob($customer, PaymentRequest $request, $calculated)
    {
        $amount_payment = (!empty($request->input('datatime')) || !empty($request->input('remote_pay'))) ? $calculated['sum'] : (float)$request->input('amount_payment', 0);


        $job = $customer->jobs()->create([
            'order_date'      => !empty($request->input('datatime')) ? date('Y-m-d H:i:s', strtotime($request->input('datatime'))) : null,
            'from'            => $request->input('start'),
            'to'              => $request->input('finish'),
            'passengers'      => $request->input('passengers', 1),
            'passengers_fee'  => $calculated['sum_extra_passengers'],
            'baggage_seats'   => $request->input('baggage_seats', 0),
            'baggage_fee'     => $request->input('baggage_fee', 0),
            'distance'        => $calculated['distance'],
            'travel_time'     => $calculated['duration'],
            'cab_number'      => $request->input('cab_number', 0),
            'amount_payment'  => $amount_payment,
            'amount_tip'      => (int)$request->input('amount_tip', 0),
            'amount_total'    => $amount_payment + (float)$request->input('amount_tip', 0),
            'operation_cost'  => $request->input('operation_cost', 0),
            'payment_type'    => $request->input('payment_method', 'card'),
            'advance_booking' => (!empty($request->input('datatime'))) ? 1 : 0,
            'send_notify'     => (!empty($request->input('send_notify'))) ? 1 : 0,
            'comments'        => $request->input('comments'),
            'user_timezone'   => timezone_name_from_abbr("", $request->input('user_timezone_offset') * 60, false)
        ]);

        return $job;
    }

    public static function saveTransaction($customer, $job, $transactionData, PaymentRequest $request)
    {
        $transaction = new \App\Models\Transaction([
            'card_holder_name'    => $request->input('payer_name'),
            'card_number'         => $request->input('credit_card_number'),
            'transaction_id'      => $transactionData['TransactionID'],
            'action_code'         => $transactionData['ActionCode'],
            'approval'            => $transactionData['Approval'],
            'response_text'       => $transactionData['ResponseText'],
            'verification_result' => $transactionData['VerificationResult'],
        ]);
        $transaction->customer()->associate($customer);
        $transaction->job()->associate($job);
        $transaction->save();

        return $transaction;
    }
}
