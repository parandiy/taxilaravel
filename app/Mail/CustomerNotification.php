<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerNotification
    extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $job;
    public $transaction;
    public $jobType;

    public function __construct($customer, $job, $transaction, $jobType)
    {
        $this->customer    = $customer;
        $this->job         = $job;
        $this->transaction = $transaction;
        $this->jobType     = $jobType;
    }

    public function build()
    {
        return $this->markdown('mail.customer_notification', ['customer' => $this->customer, 'job' => $this->job, 'transaction' => $this->transaction, 'jobType' => $this->jobType]);
    }
}
