<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest
    extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'payment_method'     => 'required',
            'payer_name'         => 'required_if:payment_method,card',
            'credit_card_number' => 'required_if:payment_method,card',
            'expiration_month'   => 'required_if:payment_method,card',
            'expiration_year'    => 'required_if:payment_method,card',
            'card_number_cvv'    => 'required_if:payment_method,card',
            'user_name'          => 'required_if:payment_method,cash',
            'user_email'         => 'required|email|same:user_confirm_email',
            'user_phone'         => 'required',
            'user_address'       => 'nullable',
            'user_city'          => 'nullable',
            'user_zipcode'       => 'nullable',
            'datatime'           => 'nullable',
            'start'              => 'required',
            'finish'             => 'required',
            'passengers'         => 'nullable|integer',
            'passengers_fee'     => 'nullable',
            'baggage_seats'      => 'nullable',
            'baggage_fee'        => 'nullable',
            'cab_number'         => 'nullable',
            'amount_payment'     => 'required_if:payment_method,cash',
            'amount_tip'         => 'nullable',
            'operation_cost'     => 'nullable',
            'comments'           => 'nullable',
            'state_id'           => 'nullable',
            'i_agree'            => 'required',
            'send_notify'        => 'nullable',
        ];

        if (empty($this->get('remote_pay'))) {
            $rules[recaptchaFieldName()] = recaptchaRuleName();
        }

        return $rules;
    }
}
