<?php

namespace App\Http\Controllers;

use App\Actions\CalculationActions;
use App\Actions\OrderNotificationActions;
use App\Actions\OrderSaveActions;
use App\Http\Requests\PaymentRequest;
use App\Mail\CustomerNotification;
use App\Mail\CustomerPaymentNotification;
use App\Mail\OperatorNotification;
use App\Models\Customer;
use App\Models\Job;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Log;

class ScheduleController
    extends Controller
{
    public function create()
    {
        return view('schedule.create');
    }

    public function store(PaymentRequest $request)
    {
        /*$creditCardData = [
            'firstName'   => 'Joe',
            'lastName'    => 'Smith',
            'number'      => '4000300020001000',
            'expiryMonth' => '05',
            'expiryYear'  => '20',
            'cvv'         => '123'
        ];*/

        $isSuccess   = false;
        $isRemotePay = !empty($request->input('remote_pay'));

        $calculated = CalculationActions::calculate($request->input('start'), $request->input('finish'), $request->input('passengers', 1));
        if ($request->input('payment_method') == 'card') {
            $names          = explode(' ', $request->input('payer_name'));
            $creditCardData = [
                'firstName'   => $names[0],
                'lastName'    => $names[1],
                'number'      => $request->input('credit_card_number'),
                'expiryMonth' => $request->input('expiration_month'),
                'expiryYear'  => $request->input('expiration_year'),
                'cvv'         => $request->input('card_number_cvv')
            ];

            $needCalcuated = (!empty($request->input('datatime')) || $isRemotePay);
            $amount_total  = ($needCalcuated) ? $calculated['sum'] + (float)$request->input('amount_tip', 0) : (float)$request->input('amount_payment',
                    0) + (float)$request->input('amount_tip', 0);

            $token           = \App\Services\Ippay\Helper::getToken($creditCardData);
            $transactionData = \App\Services\Ippay\Helper::purchase($token, $amount_total);

            if (!empty($transactionData)) {
                $customer    = OrderSaveActions::saveCustomer($request);
                $job         = OrderSaveActions::saveJob($customer, $request, $calculated);
                $transaction = OrderSaveActions::saveTransaction($customer, $job, $transactionData, $request);

                $isSuccess = true;
            } else {
                $amount_payment = (!empty($request->input('datatime')) || $isRemotePay) ? $calculated['sum'] : (float)$request->input('amount_payment', 0);
                $mail_data      = [
                    'start'          => $request->input('start'),
                    'finish'         => $request->input('finish'),
                    'amount_payment' => $amount_payment,
                    'amount_tip'     => $request->input('amount_tip', 0),
                    'comments'       => $request->input('comments'),
                ];
                Mail::to($request->input('user_email'))->send(new CustomerPaymentNotification($mail_data));
                $isSuccess = true;
            }
        } else {
            $customer    = OrderSaveActions::saveCustomer($request);
            $job         = OrderSaveActions::saveJob($customer, $request, $calculated);
            $transaction = null;
            $isSuccess   = true;
        }

        if ($isSuccess) {
            $jobType = (!empty($request->input('datatime'))) ? 'Scheduled' : '';

            try {
                Mail::to($customer->email)->send(new CustomerNotification($customer, $job, $transaction, $jobType));
            } catch (\Exception $e) {
                Log::error('Message not sent to email: ' . $customer->email);
                Log::error($e->getMessage());
            }
            try {
                Mail::to(config('taxi.operator_email'))->send(new OperatorNotification($customer, $job, $transaction, $jobType));
            } catch (\Exception $e) {
                Log::error('Message not sent to email: ' . config('taxi.operator_email'));
                Log::error($e->getMessage());
            }

            $smsOperator  = OrderNotificationActions::sendSmsToOperator($customer, $job, $transaction);
            $smsPassenger = OrderNotificationActions::sendSmsToCustomer($customer, $job, $transaction);

            $input                    = $request->all();
            $input['payment_success'] = true;
            $input['transaction_id']  = (!empty($transaction)) ? $transaction->id : null;
            $input['amount_payment']  = $amount_total;
            $input['amount_total']    = $amount_total;

            return ($isRemotePay) ? redirect()->route('order.create')->withInput($input) : [
                "smsPassenger"   => ["error" => false, "success" => $smsOperator],
                "smsOperator"    => ["error" => false, "success" => $smsPassenger],
                "transaction_id" => (!empty($transaction)) ? $transaction->id : null,
                "payment"        => true
            ];
        }

        $input                    = $request->all();
        $input['payment_success'] = false;
        $input['amount_payment']  = $amount_total;

        return ($isRemotePay) ? redirect()->route('order.create')->withInput($input) : [
            'errors' => [
                'Something went wrong'
            ]
        ];
    }

    public function getOrderForm(Request $request)
    {
        $states = State::pluck('abbrev', 'id')->toArray();

        return [
            'errors'     => false,
            'errors_msg' => [],
            'template'   => view('schedule.order_form', compact('states'))->render()
        ];
    }

    public function testform()
    {
        return view('schedule.testform');
    }
}
