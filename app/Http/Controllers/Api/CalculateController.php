<?php

namespace App\Http\Controllers\Api;

use App\Actions\CalculationActions;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use TeamPickr\DistanceMatrix\DistanceMatrix;
use TeamPickr\DistanceMatrix\Licenses\StandardLicense;

class CalculateController
    extends Controller
{
    public function index(Request $request)
    {
        $origin      = $request->input('origin');
        $destination = $request->input('destination');

        $result = CalculationActions::calculate($origin, $destination);

        if($result)
        {
            return $result;
        }

        return [
            'result' => 'fail'
        ];
    }
}
