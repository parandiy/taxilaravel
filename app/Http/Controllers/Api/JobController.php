<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Job;
use Spatie\ArrayToXml\ArrayToXml;

class JobController
    extends Controller
{
    public function index()
    {
        $jobs        = Job::viewed(0)->where('payment_type', 'card')->limit(1)->get();
        $data['job'] = [];

        foreach ($jobs as $job) {
            $job->update(['viewed' => 1]);
            $data['job'][] = $job->toXmlData();
        }
        $result = ArrayToXml::convert($data);

        return response($result, 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    public function show(Job $job)
    {
        $data = $job->toXmlData();

        $result = ArrayToXml::convert($data);

        return response($result, 200, [
            'Content-Type' => 'application/xml'
        ]);
    }
}
