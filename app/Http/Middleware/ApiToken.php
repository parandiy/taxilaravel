<?php

namespace App\Http\Middleware;

use Closure;
use Config;
class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('access_token', '') != config('taxi.access_token')) {
            die('Unauthorized.') ;
        }

        return $next($request);
    }
}
