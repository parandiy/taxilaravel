<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/terms', function () {
    return view('terms');
})->name('terms');

Route::get('/privacy-policy', function () {
    return view('privacy');
})->name('privacy');

Route::resource('order', 'OrderController')->only(['create', 'store']);
Route::resource('schedule', 'ScheduleController')->only(['create', 'store']);
Route::get('schedule/getOrderForm', 'ScheduleController@getOrderForm')->name('schedule.getOrderForm');
Route::get('schedule/testform', 'ScheduleController@testform')->name('schedule.testform');
Route::get('mailable', function () {
    return new App\Mail\CustomerPaymentNotification([]);
});
