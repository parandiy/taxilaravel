<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('jobs', 'Api\JobController')->middleware('auth.token')->only(['index', 'show']);


Route::middleware('auth:api')->group(function () {
    Route::get('calculate', 'Api\CalculateController@index')->name('calculate');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
