<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable
    extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->timestamp('order_date')->nullable();
            $table->text('from');
            $table->text('to');
            $table->integer('passengers');
            $table->float('passengers_fee');
            $table->integer('baggage_seats');
            $table->float('baggage_fee');
            $table->float('distance');
            $table->integer('travel_time');
            $table->integer('cab_number')->nullable();
            $table->float('amount_payment');
            $table->float('amount_tip');
            $table->float('amount_total');
            $table->float('operation_cost');
            $table->text('comments')->nullable();
            $table->boolean('send_notify')->default(0);
            $table->boolean('advance_booking')->default(0);
            $table->enum('payment_type', ['card', 'cash'])->default('card');
            $table->boolean('viewed')->default(0);
            $table->string('user_timezone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
