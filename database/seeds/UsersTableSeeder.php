<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder
    extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'name'      => 'test',
            'email'     => 'test@test.com',
            'password'  => Hash::make(123456),
            'api_token' => 'PARCrX0ziIK175Z3biuB4WtvwGjPNuUJCGluVqrbCyr282zLwATfBsDZwW17'
        ];
        \App\User::create($user);
    }
}
