<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder
    extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = array(
            array('id' => '1', 'name' => 'Alaska', 'abbrev' => 'AK'),
            array('id' => '2', 'name' => 'Alabama', 'abbrev' => 'AL'),
            array('id' => '3', 'name' => 'American Samoa', 'abbrev' => 'AS'),
            array('id' => '4', 'name' => 'Arizona', 'abbrev' => 'AZ'),
            array('id' => '5', 'name' => 'Arkansas', 'abbrev' => 'AR'),
            array('id' => '6', 'name' => 'California', 'abbrev' => 'CA'),
            array('id' => '7', 'name' => 'Colorado', 'abbrev' => 'CO'),
            array('id' => '8', 'name' => 'Connecticut', 'abbrev' => 'CT'),
            array('id' => '9', 'name' => 'Delaware', 'abbrev' => 'DE'),
            array('id' => '10', 'name' => 'District of Columbia', 'abbrev' => 'DC'),
            array('id' => '11', 'name' => 'Federated States of Micronesia', 'abbrev' => 'FM'),
            array('id' => '12', 'name' => 'Florida', 'abbrev' => 'FL'),
            array('id' => '13', 'name' => 'Georgia', 'abbrev' => 'GA'),
            array('id' => '14', 'name' => 'Guam', 'abbrev' => 'GU'),
            array('id' => '15', 'name' => 'Hawaii', 'abbrev' => 'HI'),
            array('id' => '16', 'name' => 'Idaho', 'abbrev' => 'ID'),
            array('id' => '17', 'name' => 'Illinois', 'abbrev' => 'IL'),
            array('id' => '18', 'name' => 'Indiana', 'abbrev' => 'IN'),
            array('id' => '19', 'name' => 'Iowa', 'abbrev' => 'IA'),
            array('id' => '20', 'name' => 'Kansas', 'abbrev' => 'KS'),
            array('id' => '21', 'name' => 'Kentucky', 'abbrev' => 'KY'),
            array('id' => '22', 'name' => 'Louisiana', 'abbrev' => 'LA'),
            array('id' => '23', 'name' => 'Maine', 'abbrev' => 'ME'),
            array('id' => '24', 'name' => 'Marshall Islands', 'abbrev' => 'MH'),
            array('id' => '25', 'name' => 'Maryland', 'abbrev' => 'MD'),
            array('id' => '26', 'name' => 'Massachusetts', 'abbrev' => 'MA'),
            array('id' => '27', 'name' => 'Michigan', 'abbrev' => 'MI'),
            array('id' => '28', 'name' => 'Minnesota', 'abbrev' => 'MN'),
            array('id' => '29', 'name' => 'Mississippi', 'abbrev' => 'MS'),
            array('id' => '30', 'name' => 'Missouri', 'abbrev' => 'MO'),
            array('id' => '31', 'name' => 'Montana', 'abbrev' => 'MT'),
            array('id' => '32', 'name' => 'Nebraska', 'abbrev' => 'NE'),
            array('id' => '33', 'name' => 'Nevada', 'abbrev' => 'NV'),
            array('id' => '34', 'name' => 'New Hampshire', 'abbrev' => 'NH'),
            array('id' => '35', 'name' => 'New Jersey', 'abbrev' => 'NJ'),
            array('id' => '36', 'name' => 'New Mexico', 'abbrev' => 'NM'),
            array('id' => '37', 'name' => 'New York', 'abbrev' => 'NY'),
            array('id' => '38', 'name' => 'North Carolina', 'abbrev' => 'NC'),
            array('id' => '39', 'name' => 'North Dakota', 'abbrev' => 'ND'),
            array('id' => '40', 'name' => 'Northern Mariana Islands', 'abbrev' => 'MP'),
            array('id' => '41', 'name' => 'Ohio', 'abbrev' => 'OH'),
            array('id' => '42', 'name' => 'Oklahoma', 'abbrev' => 'OK'),
            array('id' => '43', 'name' => 'Oregon', 'abbrev' => 'OR'),
            array('id' => '44', 'name' => 'Palau', 'abbrev' => 'PW'),
            array('id' => '45', 'name' => 'Pennsylvania', 'abbrev' => 'PA'),
            array('id' => '46', 'name' => 'Puerto Rico', 'abbrev' => 'PR'),
            array('id' => '47', 'name' => 'Rhode Island', 'abbrev' => 'RI'),
            array('id' => '48', 'name' => 'South Carolina', 'abbrev' => 'SC'),
            array('id' => '49', 'name' => 'South Dakota', 'abbrev' => 'SD'),
            array('id' => '50', 'name' => 'Tennessee', 'abbrev' => 'TN'),
            array('id' => '51', 'name' => 'Texas', 'abbrev' => 'TX'),
            array('id' => '52', 'name' => 'Utah', 'abbrev' => 'UT'),
            array('id' => '53', 'name' => 'Vermont', 'abbrev' => 'VT'),
            array('id' => '54', 'name' => 'Virgin Islands', 'abbrev' => 'VI'),
            array('id' => '55', 'name' => 'Virginia', 'abbrev' => 'VA'),
            array('id' => '56', 'name' => 'Washington', 'abbrev' => 'WA'),
            array('id' => '57', 'name' => 'West Virginia', 'abbrev' => 'WV'),
            array('id' => '58', 'name' => 'Wisconsin', 'abbrev' => 'WI'),
            array('id' => '59', 'name' => 'Wyoming', 'abbrev' => 'WY'),
            array('id' => '60', 'name' => 'Armed Forces Africa', 'abbrev' => 'AE'),
            array('id' => '61', 'name' => 'Armed Forces Americas (except Canada)', 'abbrev' => 'AA'),
            array('id' => '62', 'name' => 'Armed Forces Canada', 'abbrev' => 'AE'),
            array('id' => '63', 'name' => 'Armed Forces Europe', 'abbrev' => 'AE'),
            array('id' => '64', 'name' => 'Armed Forces Middle East', 'abbrev' => 'AE'),
            array('id' => '65', 'name' => 'Armed Forces Pacific', 'abbrev' => 'AP')
        );

        DB::table('states')->insert($states);
    }
}
