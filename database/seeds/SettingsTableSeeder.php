<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder
    extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'key'   => 'passenger_fee',
                'value' => '0.00'
            ],
            [
                'key'   => 'price_per_mile',
                'value' => '2.50'
            ],
            [
                'key'   => 'pay_per_seconds',
                'value' => '48'
            ],
            [
                'key'   => 'pay_per_seconds_sum',
                'value' => '0.50'
            ],
            [
                'key'   => 'price_extra_passenger',
                'value' => '2.00'
            ],
        ];
        DB::table('settings')->insert($settings);
    }
}
