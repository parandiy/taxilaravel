$(document).ready(function () {

    $('.costedParameters').hide();

    var oldDatetimeValue = '01/01/2015 00:00';

    $(document).on('click', '.main__get-order-form', function () {

        var sum = parseFloat($('.sum').text()),
            start = $('input#pac-input').val(),
            finish = $('input#pac-input2').val(),
            datatime = $('input[name=datetime]').val(),
            comments = $('input[name=comments]').val(),
            amountTip = $('input[name=amount_tip]').val(),
            tTime = $('.time').text() + ' ' + $('#unit').text(),
            passengers = $('#passengers-input').val(),
            distance = $('.miles').text();

        if (sum > 0) {

            gt_payment.showPaymentFormPopup(sum, start, finish, datatime, comments, tTime, distance, passengers, amountTip);

        }

    });

    $(document).on('click', '.main__get-order-schedule-form', function () {
        gt_payment.confirmScheduledForm();
    })

    makePaymentToggle();

    /*$(document).click(function(e){

        newDateTime = $('.order__form .datetime').val();

        console.log(oldDatetimeValue + ';');
        console.log(newDateTime);

        if(oldDatetimeValue && oldDatetimeValue !== newDateTime) {

            if(Date.parse(oldDatetimeValue) < Date.parse(newDateTime)) {

                $('.main__get-order-form').text('Schedule a taxi Cab Ride');

            }

            oldDatetimeValue = newDateTime;

        }
    });*/

    $(document).on('click touchstart', '.placeholder-mobile', function () {
        $(this).hide();
    });

    $(document).on('change', '#datetimepicker-mobile', function () {
        var value = $(this).val();
        if (value === '') {
            $('.placeholder-mobile').show();
        }
    });


    gt_payment.calculateSumm();
    $(document).on('change keyup', 'input[name=paymentAmount]', function () {
        gt_payment.calculateSumm();
    })

    $(document).on('change keyup', 'input[name=amount_tip]', function () {
        if($('input[name=amount_tip]').val() < 0)
        {
            $('input[name=amount_tip]').val(0);
        }
        gt_payment.calculateSumm();
    })

    $(document).on('focusout', 'input[name=paymentAmount], input[name=amount_tip]', function () {
        var amount = $('input[name=paymentAmount]').val();
        var tip = $('input[name=amount_tip]').val();
        if (typeof (amount) !== 'NaN' && amount != '') {
            $('input[name=paymentAmount]').val(parseFloat(amount).toFixed(2).toString().replace(/,/g, '.'));
        }
        if (typeof (tip) !== 'NaN' || tip != '') {
            $('input[name=amount_tip]').val(parseFloat(tip).toFixed(2).toString().replace(/,/g, '.'));
        }
    })

});

$(document).on('change', '.order__form .datetime', function () {
    var now = new Date();
    console.log(now);
});

var gt_payment = {

    showPaymentFormPopup: function (sum, start, finish, datatime, comments, tTime, distance, passengers, amountTip) {

        $.get('/schedule/getOrderForm', {
            'amount_payment': sum,
            'start': start,
            'finish': finish,
            'datatime': datatime,
            'comments': comments,
            'tTime': tTime,
            'distance': distance,
            passengers: passengers,
            amount_tip: amountTip,
            user_timezone_offset: getUserTimezone()
        }, function (response) {

            if (response.template) {
                var btnText = (datatime !== '') ? 'Reserve Cab' : 'Pay Now';
                paymentFormPopup = BootstrapDialog.show({
                    title: '<img src="/img/creditcards.png" width="140px" alt="" />',
                    message: $('<div class="payment"></div>').html(response.template),
                    buttons: [{
                        id: 'btn-payment-form-ok',
                        icon: 'fa fa-arrow-right',
                        label: '$' + ((Math.round(sum * 100) / 100).toFixed(2)) + ' - ' + btnText,
                        cssClass: 'order__submit-order btn btn-primary text-bold text-uppercase btn-full',
                        autospin: false,
                        action: function () {
                            gt_payment.submitOrderForm(sum, start, finish, datatime);
                        }
                    }],
                    onshown: function () {
                        $('#user_phone').mask('(999) 999-9999');
                        initRecaptcha();
                        //$('#credit_card_number').mask('9999-9999-9999-9999');
                        //$('#card_number_cvv').mask('999');
                        //$('#user_zipcode').mask('99999');
                    }
                });

            }

        }, 'json');

    },

    submitOrderForm: function (sum, start, finish, datatime) {

        $('.field-error').removeClass('field-error ');

        preLoader('Loading');

        $.post('/schedule', $('#orderForm').serializeArray(),
            function (response) {

                overlay.hide();

                if (response.errors_msg) {

                    if (typeof response.errors_msg === 'object' || typeof response.errors_msg === 'array') {

                        var html = '<p align="center"><img src="/img/allerticon.png" alt="" /></p>';
                        html += 'Errors:<br/>';

                        for (var err in response.errors_msg) {

                            if (typeof (response.errors_msg[err].attribute_label) === 'undefined') {
                                html += response.errors_msg[err] + '<br/>';
                            } else {
                                html += response.errors_msg[err].attribute_label + ': ' + response.errors_msg[err].error_text + '<br/>';
                            }

                            $('#' + err).addClass('field-error ');


                        }

                        BootstrapDialog.show({
                            title: 'Oops!',
                            message: $('<div></div>').html(html),
                            cssClass: 'modal-dialog-margin-25',
                            buttons: [{
                                id: 'btn-transaction-result-cancel',
                                icon: 'fa fa-check',
                                label: 'Back to form',
                                cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-red',
                                autospin: false,
                                action: function (dialogRef) {

                                    dialogRef.close();


                                }
                            }]
                        });

                    }

                } else if (response.payment) {

                    var html = '<p align="center"><img src="/img/successicon.png" alt="" /></p>';
                    html += '<p align="center"><b>Payment successfull!</b></p>';
                    html += '<p align="center">\n\
                                Autorization Number: ' + response.transaction_id + ', <br />\n\
                                Pick-up: ' + start + ', <br />\n\
                                Drop-off: ' + finish + ', <br />\n\
                                Amount: $' + parseFloat(sum).toFixed(2) + ', <br />\n\
                                Date and time: ' + datatime + '\n\
                             </p>';
                    html += '<p align="center">Your payment has been processed. <br /> Please look out for your taxi cab.</p>';


                    paymentFormPopup.close();
                    BootstrapDialog.show({
                        title: 'Success',
                        message: $('<div></div>').html(html),
                        cssClass: 'modal-dialog-250',
                        buttons: [{
                            id: 'btn-transaction-result-ok',
                            icon: 'fa fa-check',
                            label: 'Back to form',
                            cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-green',
                            autospin: false,
                            action: function (dialogRef) {
                                dialogRef.close();
                                window.location.href = '/';

                            }
                        }]
                    });

                }

            },
            'json').fail(function (response) {

            console.log(response.responseJSON);
            overlay.hide();
            var html = '<p align="center"><img src="/img/allerticon.png" alt="" /></p>';
            html += 'Errors:' + response.responseJSON.message + '<br/>';

            for (var err in response.responseJSON.errors) {

                html += response.responseJSON.errors[err].join(' ') + '<br/>';

                $('#' + err).addClass('field-error ');
            }

            BootstrapDialog.show({
                title: 'Oops!',
                message: $('<div></div>').html(html),
                cssClass: 'modal-dialog-margin-25',
                buttons: [{
                    id: 'btn-transaction-result-cancel',
                    icon: 'fa fa-check',
                    label: 'Back to form',
                    cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-red',
                    autospin: false,
                    action: function (dialogRef) {

                        dialogRef.close();


                    }
                }]
            });
        });

    },

    calculateSumm: function (input) {
        var amount = $('input[name=paymentAmount]').val();
        var tip = $('input[name=amount_tip]').val();
        if (typeof (amount) === 'NaN' || amount == '') {
            amount = 0;
        }
        if (typeof (tip) === 'NaN' || tip == '') {
            tip = 0;
        }
        var sum = parseFloat(amount) + parseFloat(tip);
        $('.cost').html('$ ' + sum.toFixed(2));
    },

    confirmScheduledForm: function () {
        var start = $('input#pac-input').val();
        var finish = $('input#pac-input2').val();
        var cabNumber = $('input[name=cabNumber]').val();
        var comments = $('input[name=comments]').val();
        var paymentAmount = $('input[name=paymentAmount]').val();
        var amount_tip = $('input[name=amount_tip]').val();
        var passengers = $('#passengers-input').val();
        if (typeof (paymentAmount) === 'NaN' || paymentAmount == '') {
            paymentAmount = 0;
        }
        if (typeof (amount_tip) === 'NaN' || amount_tip == '') {
            amount_tip = 0;
        }
        var sum = parseFloat(paymentAmount) + parseFloat(amount_tip);
        var arr = [];
        start == '' ? arr.push('start') : '';
        finish == '' ? arr.push('finish') : '';
        cabNumber == '' ? arr.push('Cab number') : '';
        paymentAmount == '' ? arr.push('payment Amount') : '';
        tTime = $('.time').text() + ' ' + $('#unit').text();
        distance = $('.miles').text();
        if (arr == '') {
            gt_payment.showSchedulePaymentForm(start, finish, cabNumber, comments, paymentAmount, amount_tip, sum, tTime, distance, passengers);
        } else {
            var html = '<p align="center"><img src="/img/allerticon.png" alt="" /></p>';
            html += 'Fill in these fields: ';
            html += arr;
            BootstrapDialog.show({
                title: 'Oops!',
                message: $('<div></div>').html(html),
                cssClass: 'modal-dialog-margin-25',
                buttons: [{
                    id: 'btn-transaction-result-cancel',
                    icon: 'fa fa-check',
                    label: 'Back to form',
                    cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-red',
                    autospin: false,
                    action: function (dialogRef) {

                        dialogRef.close();


                    }
                }]
            });
        }
    },

    showSchedulePaymentForm: function (start, finish, cabNumber, comments, paymentAmount, amount_tip, sum, tTime, distance, passengers) {
        $.get('/schedule/getOrderForm', {
            start: start,
            finish: finish,
            cabNumber: cabNumber,
            comments: comments,
            amount_payment: paymentAmount,
            amount_tip: amount_tip,
            amount_total: sum,
            tTime: tTime,
            distance: distance,
            passengers: passengers,
            user_timezone_offset: getUserTimezone()
        }, function (response) {

            var btnText = 'Pay Now';
            if (response.template) {
                paymentFormPopup = BootstrapDialog.show({
                    title: '<img src="/img/creditcards.png" width="140px" alt="" />',
                    message: $('<div class="payment"></div>').html(response.template),
                    buttons: [{
                        id: 'btn-payment-form-ok',
                        icon: 'fa fa-arrow-right',
                        label: '$' + ((Math.round(sum * 100) / 100).toFixed(2)) + ' - ' + btnText,
                        cssClass: 'order__submit-order-schedule btn btn-primary text-bold text-uppercase btn-full',
                        autospin: false,
                        action: function () {
                            gt_payment.submitScheduleForm(start, finish, cabNumber, comments, paymentAmount, amount_tip, sum)
                        }
                    }],
                    onshown: function () {
                        $('#user_phone').mask('(999) 999-9999');
                        initRecaptcha();
                        //$('#credit_card_number').mask('9999-9999-9999-9999');
                        //$('#card_number_cvv').mask('999');
                        //$('#user_zipcode').mask('99999');
                    }
                });

            }
        }, 'json')
    },

    submitScheduleForm: function (start, finish, cabNumber, comments, paymentAmount, amount_tip, sum) {

        $('.field-error').removeClass('field-error ');

        preLoader('Loading');


        $.post('/schedule', $('#orderForm').serializeArray(),
            function (response) {

                overlay.hide();

                if (response.errors) {

                    if (typeof response.errors_msg === 'object' || typeof response.errors_msg === 'array') {

                        var html = '<p align="center"><img src="/img/allerticon.png" alt="" /></p>';
                        html += 'Errors:<br/>';

                        for (var err in response.errors_msg) {

                            if (typeof (response.errors_msg[err].attribute_label) === 'undefined') {
                                html += response.errors_msg[err] + '<br/>';
                            } else {
                                html += response.errors_msg[err].attribute_label + ': ' + response.errors_msg[err].error_text + '<br/>';
                            }

                            $('#' + err).addClass('field-error ');


                        }

                        BootstrapDialog.show({
                            title: 'Oops!',
                            message: $('<div></div>').html(html),
                            cssClass: 'modal-dialog-margin-25',
                            buttons: [{
                                id: 'btn-transaction-result-cancel',
                                icon: 'fa fa-check',
                                label: 'Back to form',
                                cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-red',
                                autospin: false,
                                action: function (dialogRef) {

                                    dialogRef.close();


                                }
                            }]
                        });

                    }

                } else if (response.payment) {

                    var html = '<p align="center"><img src="/img/successicon.png" alt="" /></p>';
                    html += '<p align="center"><b>Payment successfull!</b></p>';
                    html += '<p align="center">\n\
                                Authorization Number: ' + response.transaction_id + ', <br />\n\
                                Pick-up: ' + start + ', <br />\n\
                                Drop-off: ' + finish + ', <br />\n\
                                Amount: $' + parseFloat(sum).toFixed(2) + ', <br />\n\
                             </p>';
                    html += '<p align="center">Your payment has been processed. <br /> Please look out for your taxi cab.</p>';

                    paymentFormPopup.close();
                    BootstrapDialog.show({
                        title: 'Success',
                        message: $('<div></div>').html(html),
                        cssClass: 'modal-dialog-250',
                        buttons: [{
                            id: 'btn-transaction-result-ok',
                            icon: 'fa fa-check',
                            label: 'Back to form',
                            cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-green',
                            autospin: false,
                            action: function (dialogRef) {

                                dialogRef.close();
                                document.location.href = '/';

                            }
                        }]
                    });

                }

            },
            'json').fail(function (response) {

            overlay.hide();
            var html = '<p align="center"><img src="/img/allerticon.png" alt="" /></p>';
            html += 'Errors:' + response.responseJSON.message + '<br/>';

            for (var err in response.responseJSON.errors) {

                html += response.responseJSON.errors[err].join(' ') + '<br/>';

                $('#' + err).addClass('field-error ');
            }

            BootstrapDialog.show({
                title: 'Oops!',
                message: $('<div></div>').html(html),
                cssClass: 'modal-dialog-margin-25',
                buttons: [{
                    id: 'btn-transaction-result-cancel',
                    icon: 'fa fa-check',
                    label: 'Back to form',
                    cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-red',
                    autospin: false,
                    action: function (dialogRef) {

                        dialogRef.close();


                    }
                }]
            });
        });

    }


};

function initRecaptcha() {
    var captchaWidgetId = grecaptcha.render('recaptcha-element', {
        'sitekey': $('#recaptcha-element').data('sitekey'),  // required
        'theme': 'light',  // optional
        //'callback': 'verifyCallback'  // optional
    });
}

function makePaymentToggle() {
    $('body').on('change', '.payment-method-toggle', function () {
        var sum = parseFloat($('.sum').text());

        if ($(this).val() == 'cash') {
            payText = '$' + sum + ' – Pay Cash before Departure.';
            $('#payment_method_block').hide();
            $('#payment_cash_block').show();
            $('#btn-payment-form-ok').data('prevtext', $('#btn-payment-form-ok').text()).text(payText);
        } else {
            $('#payment_method_block').show();
            $('#payment_cash_block').hide();
            $('#btn-payment-form-ok').text($('#btn-payment-form-ok').data('prevtext')).data('prevtext', '');
        }
    });
}

function getUserTimezone() {
    var timezone_offset_minutes = new Date().getTimezoneOffset();
    timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

    return timezone_offset_minutes;
}

function showSuccessPayment(transaction_id, start, finish, sum) {
    var html = '<p align="center"><img src="/img/successicon.png" alt="" /></p>';
    html += '<p align="center"><b>Payment successfull!</b></p>';
    html += '<p align="center">\n\
                                Authorization Number: ' + transaction_id + ', <br />\n\
                                Pick-up: ' + start + ', <br />\n\
                                Drop-off: ' + finish + ', <br />\n\
                                Amount: $' + parseFloat(sum).toFixed(2) + ', <br />\n\
                             </p>';
    html += '<p align="center">Your payment has been processed. <br /> Please look out for your taxi cab.</p>';

    BootstrapDialog.show({
        title: 'Success',
        message: $('<div></div>').html(html),
        cssClass: 'modal-dialog-250',
        buttons: [{
            id: 'btn-transaction-result-ok',
            icon: 'fa fa-check',
            label: 'Back to form',
            cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-green',
            autospin: false,
            action: function (dialogRef) {
                dialogRef.close();
                window.location.href = '/';

            }
        }]
    });
}

function showErrorPayment(message, errors) {
    var html = '<p align="center"><img src="/img/allerticon.png" alt="" /></p>';
    html += 'Errors: something went wrong. You can retry by enter your card details.<br/>';

    BootstrapDialog.show({
        title: 'Oops!',
        message: $('<div></div>').html(html),
        cssClass: 'modal-dialog-margin-25',
        buttons: [{
            id: 'btn-transaction-result-cancel',
            icon: 'fa fa-check',
            label: 'Back to form',
            cssClass: 'btn btn-primary text-bold text-uppercase btn-full text-red',
            autospin: false,
            action: function (dialogRef) {

                dialogRef.close();


            }
        }]
    });
}
