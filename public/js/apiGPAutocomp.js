function calculation(){
    $('.costedParameters').hide();
        var start = $('input#pac-input').val().replace(/ /g,'+'),
            finich = $('input#pac-input2').val().replace(/ /g,'+'),
            passengers = parseInt($('#passengers-input').val()),
            amountTip = parseInt($('#input-amount-tip').val()),
        //  key = 'AIzaSyDgjGB2ci6KzkoUtQubIK1b6DU-Do-hv6A',
            directionsService = new google.maps.DirectionsService(),
            //oneml = '1.48',
            mil = '1609',
            time, sum, miles;

        if(isNaN(amountTip))
        {
            amountTip = 0;
        }
        if(start !== '' && finich !== ''){

            var preLoder = preLoader('Loading');
            $('.costedParameters').show();

            var request = {
                origin:start,
                destination:finich,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function(response, status) {
              if (status === google.maps.DirectionsStatus.OK) {

                var Di = response.routes[0].legs[0].distance.value,
                    Mi = response.routes[0].legs[0].duration.value,
                    hours, minutes;

                miles = (Di/mil).toFixed(2) ;

                if(Mi > 3600){
                    hours = Math.floor(Mi/3600);
                    minutes = ((Mi - (hours*3600))/60).toFixed();

                        if(minutes<10){
                            time = hours + '.0' + minutes;
                        }else{
                            time = hours + '.' + minutes;
                        }

                    $('#unit').text('Hours');
                }else{
                    time = (Mi/60).toFixed();
                    $('#unit').text('Min');
                }

                var sumPerTime = (Mi/payPerSeconds)*payPerSecondsSum;
                var sumPerMiles = (miles*oneml);
                var sumPerExtraPassengers = ((passengers-1) * parseInt(payPerExtraPassengerSum));
                sum = (sumPerTime+sumPerMiles+passengerFee + sumPerExtraPassengers + amountTip).toFixed(2);

                overlay.hide();
                $('.miles').text(miles);
                $('.time').text(time);
                $('.sum').text(sum);
                $('#input-travel-time').val(Mi);

              }else{
                  overlay.hide();
                  alert('It is impossible to find a current way');
              }
            });

        }

}

var overlay = {};

function preLoader(text, second){
    	var opts = {
		lines: 13, // The number of lines to draw
		length: 11, // The length of each line
		width: 5, // The line thickness
		radius: 17, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		color: '#FFF', // #rgb or #rrggbb
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: 'auto', // Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	};
	var target = document.createElement("div");
	document.body.appendChild(target);
	var spinner = new Spinner(opts).spin(target);


        if(second) {
            overlay = iosOverlay({
		text: text,
		spinner: spinner,
                duration: second
            });
        } else {
            overlay = iosOverlay({
		text: text,
		spinner: spinner,
            });
        }

	return false;
}

function initialize() {

  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));

  var autocomplete = new google.maps.places.Autocomplete(input);
  //autocomplete.bindTo('bounds', map);

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var place = autocomplete.getPlace();
    var calc = calculation();
  });

  var input2 = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input2'));

  var autocomplete2 = new google.maps.places.Autocomplete(input2);
  //autocomplete.bindTo('bounds', map);

  google.maps.event.addListener(autocomplete2, 'place_changed', function() {
    var place = autocomplete.getPlace();
    var calc = calculation();
  });

  $('#passengers-input').on('change keyup', function() {
      var calc = calculation();
  });

    $('#input-amount-tip').on('change keyup', function() {
        var calc = calculation();
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
