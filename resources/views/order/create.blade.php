@extends('layouts.app')

@section('content')
    <form class="order__form-scheduled form-horizontal">
        <div class="main">
            <div class="bg-form">
                <div class="logo row">
                    <div class="col-xs-4">
                        <a href="/"><img src="/img/logotype.png" class="img-responsive" alt=""/></a>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/startroute.png" width="20" alt=""/></span>
                            <input type="text" class="form-control" id="pac-input"
                                   placeholder="Start / Pick-up Location"
                                   aria-describedby="inputGroupSuccess1Status"
                                   value="{{old('start') ?: request()->input('start', '')}}"
                            {{(old('remote_pay') == true && old('payment_success') == true) ? 'disabled' : ''}}>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon">&#8942;</span>
                            <hr/>
                        </div>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/endroute.png" width="20" alt=""/></span>
                            <input type="text" class="form-control" id="pac-input2"
                                   value="{{old('finish') ?: request()->input('finish', '')}}"
                                   placeholder="Finish / Drop-off Location"
                                   aria-describedby="inputGroupSuccess1Status"
                                {{(old('remote_pay') == true && old('payment_success') == true) ? 'disabled' : ''}}>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group date">
                            <span class="input-group-addon"><img src="/img/cab.png" width="20" alt=""/></span>

                            {{--<?php $detect = Yii::app()->mobileDetect; ?>
                            <?php if( $detect->isMobile() or $detect->isTablet() ): ?>--}}
                            <div class="placeholder-mobile">Cab driver id</div>
                            {{--<?php endif; ?>--}}

                            <input type="text" name="cabNumber" value="" class="form-control" placeholder="Cab driver id"
                                {{(old('remote_pay') == true && old('payment_success') == true) ? 'disabled' : ''}}/>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/comment.png" width="20" alt=""/></span>
                            <input type="text" name="comments" class="form-control"
                                   value="{{old('comments') ?: request()->input('comments', '')}}"
                                   aria-describedby="inputGroupSuccess1Status" placeholder="Comments"
                                {{(old('remote_pay') == true && old('payment_success') == true) ? 'disabled' : ''}}>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <hr/>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/pay.png" width="20" alt=""/></span>
                            <input type="text" name="paymentAmount" class="form-control"
                                   value="{{old('amount_payment') ?: request()->input('amount_payment', 0)}}"
                                   aria-describedby="inputGroupSuccess1Status" placeholder="Payment Amount"
                                {{(old('remote_pay') == true && old('payment_success') == true) ? 'disabled' : ''}}>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <hr/>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/tip.png" width="20" alt=""/></span>
                            <input type="text" name="amount_tip" class="form-control"
                                   value="{{old('amount_tip') ?: request()->input('amount_tip', 0)}}"
                                   aria-describedby="inputGroupSuccess1Status" placeholder="Amount Tip"
                                {{(old('remote_pay') == true && old('payment_success') == true) ? 'disabled' : ''}}>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>

            </div>

        </div>
        <div style="display: none;">
            <div class="costedParameters">
                <span class="time">0</span><br/> <font id="unit">Min</font>
            </div>
            <span class="miles">0.0</span>
        </div>

        <div class="total_amount">
            Total amount:<br/>
            <span class="cost"></span>
        </div>

        <div style="clear:both;"></div>


        @if(old('remote_pay', false) != true && old('payment_success', false) != true)
            <div class="col-xs-12">
                <a id="m_btn" class="main__get-order-schedule-form btn btn-primary inner-btn" href="#">CONFIRM AND
                    PAY</a>
            </div>
        @endif

    </form>
    @if(old('remote_pay') == true && old('payment_success') == true)
@section('footer_scripts')
    <script>
        showSuccessPayment('{{old('transaction_id', 0)}}', '{{old('start')}}', '{{old('finish')}}', '{{old('amount_total', 0)}}');
    </script>
@endsection
@elseif(old('remote_pay') == true && old('payment_success') == false)
@section('footer_scripts')
    <script>
        showErrorPayment('Error.', {!! json_encode($errors->all()) !!});
    </script>
@endsection
@endif
@endsection
