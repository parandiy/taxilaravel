@extends('layouts.app')

@section('content')
    <div class="top-indent">&nbsp;</div>

    <div class="main" id="pad">
        <div class="logotype"><img src="/img/logotype.png" class="img-responsive" alt=""/></div>
        <div style="clear:both;"></div>
        <div class="col-xs-12">
            <a id="m_btn" class="btn btn-primary inner-btn" href="{{route('schedule.create')}}">SCHEDULE A TRIP</a>
        </div>
        <div class="col-xs-12">
            <a id="m_btn" class="btn btn-primary inner-btn btn-ontop" href="{{route('order.create')}}">PAY FOR A
                TRIP</a>
        </div>
    </div>

    <div class="footer col-xs-12 col-md-12">
        <div class="phone-bottom">
            <!--img src="/img/phone.png" class="img-responsive" alt=""/-->
            <a href="tel:5852323232" style="font-size: 76px; text-decoration: none; color: #FFFFFF; text-shadow: 0 1px 2px rgba(0,0,0,0.5);">585-232-3232</a>
        </div>
        <div>
            Associate Taxi of Rochester, NY Inc.<br />
            790 S. Plymouth Ave<br />
            Rochester, New York 14608<br />
            (585)436-8750 Office/ (585)232-3232 Dispatch<br />
            <a style="color: #FFFFFF;" href="mailto:service@associate.taxi">service@associate.taxi</a>
        </div>
        <div>
            <iframe width="600" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=new%20york&t=&z=11&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>
        <div style="margin: 10px 0;">
            <a style="color:#ffbe25; margin-right: 10px;" href="{{route('terms')}}">Terms</a>
            <a style="color:#ffbe25;" href="{{route('privacy')}}">Privacy Policy</a>
        </div>
        <div class="text">&copy; 2020, <span> Associate Taxi of Rochester Inc</span></div>
    </div>
@endsection
