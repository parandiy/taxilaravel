@component('mail::message')
Associate Taxi of Rochester New York<br>
790 South Plymouth Ave<br>
Rochester, New York 14608<br>
(585) 232-3232<br>
www.rideshare.one<br>
info@rideshare.one<br>

# Your transaction is not approved. You can pay your order by link below.

@component('mail::button', ['url' => route('schedule.create', $mail_data)])
    Pay Order
@endcomponent

Thank you for selecting Assocaite Taxi of Rochester New York<br>

Associate Taxi of Rochester (585)232-3232.
@endcomponent
