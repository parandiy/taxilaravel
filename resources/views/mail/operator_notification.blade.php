@component('mail::message')
Associate Taxi of Rochester New York<br>
790 South Plymouth Ave<br>
Rochester, New York 14608<br>
(585) 232-3232<br>
www.rideshare.one<br>
info@rideshare.one<br>


# Job Information
Job Type:             {{$jobType}}<br>
Job Number:           {{$job->id}}<br>
Pick-up Time:         {{(!empty($job->order_date)) ? $job->order_date : ''}}<br>
Order Time:           {{$job->created_at->format('m/d/Y, g:i a')}}<br>
From:                 {{$job->from}}<br>
To:                   {{$job->to}}<br>
Passengers:           {{$job->passengers}}<br>
Passengers Fee:      ${{$job->passengers_fee}}<br>
Baggage Seats:        {{$job->baggage_seats}}<br>
Baggage Fee:         ${{$job->baggage_fee}}<br>
Distance:             {{$job->distance}} miles<br>
Travel time:          {{$job->travel_time}}<br>
Customer paid:       ${{$job->amount_total}}<br>
Operation Cost:      ${{$job->operation_cost}}<br>
Special Instructions: {{$job->comments}}<br>
Send SMS when the driver will be on the route: {{($job->send_notify) ? 'yes' : 'no'}}<br>

# Customer Information
Customer Name: {{$customer->first_name}} {{$customer->last_name}}<br>
Customer Address: {{$customer->address}}, {{$customer->city}}, {{(!empty($customer->state)) ? $customer->state->name : ''}}, {{$customer->zip_code}}<br>
Customer Email: {{$customer->email}}<br>
Customer Phone: {{$customer->phone}}<br>

@if(!empty($transaction))
# Payment Information
Card Holder Name: {{$transaction->card_holder_name}}<br>
Card Number: {{'xxxx'.substr($transaction->card_number, -4)}}<br>
Approval Code: {{$transaction->approval}}<br>
@endif
@endcomponent
