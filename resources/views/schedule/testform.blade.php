@extends('layouts.app')

@section('content')
    <form class="order__form form-horizontal" action="{{route('schedule.store')}}" method="POST">
        @if (count($errors) > 0)
            <div class="error">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="main">
            <div class="bg-form">
                <div class="logo row">
                    <div class="col-xs-4">
                        <a href="/"><img src="/img/logotype.png" class="img-responsive" alt=""/></a>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/startroute.png" width="20" alt=""/></span>
                            <input type="text" name="start" class="form-control" id="pac-input" value="6th Avenue, New York, Нью-Йорк, США"
                                   placeholder="Start / Pick-up Location" aria-describedby="inputGroupSuccess1Status">
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon">&#8942;</span>
                            <hr/>
                        </div>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/endroute.png" width="20" alt=""/></span>
                            <input type="text" name="finish" class="form-control" id="pac-input2" value="40 Wall Street, New York, Нью-Йорк, США"
                                   placeholder="Finish / Drop-off Location" aria-describedby="inputGroupSuccess1Status">
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>

            </div>
        </div>

        {{Form::hidden('payment_method', 'card')}}
        {{Form::hidden('payer_name', 'Joe Smith')}}
        {{Form::hidden('credit_card_number', '4000300020001000')}}
        {{Form::hidden('expiration_month', '12')}}
        {{Form::hidden('expiration_year', '21')}}
        {{Form::hidden('card_number_cvv', '000')}}
        {{Form::hidden('user_email', 'test1@test.ru')}}
        {{Form::hidden('user_confirm_email', 'test1@test.ru')}}
        {{Form::hidden('user_phone', '123456789')}}
        {{Form::hidden('i_agree', '1')}}
        {{Form::hidden('remote_pay', '1')}}
        {{Form::hidden('comments', 'test')}}
        {{Form::hidden('datatime', 'test')}}
        {{Form::hidden('datatime', '2020-08-20 10:00:00')}}

        <div class="col-xs-12">
            <button type="submit" class="main__get-order-form btn btn-primary inner-btn">PAY FOR TRIP</button>
        </div>

    </form>
@endsection
