@extends('layouts.app')

@section('content')
    <form class="order__form form-horizontal">

        <div class="main">
            <div class="bg-form">
                <div class="logo row">
                    <div class="col-xs-4">
                        <a href="/"><img src="/img/logotype.png" class="img-responsive" alt=""/></a>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/startroute.png" width="20" alt=""/></span>
                            <input type="text" class="form-control" id="pac-input"
                                   placeholder="Start / Pick-up Location" aria-describedby="inputGroupSuccess1Status">
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon">&#8942;</span>
                            <hr/>
                        </div>
                    </div>
                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/endroute.png" width="20" alt=""/></span>
                            <input type="text" class="form-control" id="pac-input2"
                                   placeholder="Finish / Drop-off Location" aria-describedby="inputGroupSuccess1Status">
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group date">
                            <span class="input-group-addon"><img src="/img/datetime.png" width="20" alt=""/></span>

                            {{--<?php $detect = Yii::app()->mobileDetect; ?>
                            <?php if( $detect->isMobile() or $detect->isTablet() ): ?>--}}
                            <div class="placeholder-mobile">Schedule Pick-up Time</div>
                            {{--<?php endif; ?>--}}

                            <input type="text" name="datetime" id="datetimepickerp-bootstrap" value=""
                                   class="form-control datetime" placeholder="Schedule Pick-up Time"/>
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>

                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/settings.png" width="20" alt=""/></span>
                            <select id="passengers-input" class="form-control" style="border: 0">
                                <option value="1" selected>1 Passenger</option>
                                <option value="2">2 Passengers</option>
                                <option value="3">3 Passengers</option>
                                <option value="4">4 Passengers</option>
                            </select>
                            {{--<input type="number" class="form-control" id="passengers-input" value="1"
                                   placeholder="Passengers" aria-describedby="inputGroupSuccess1Status" min="1" max="4">--}}
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>


                <div class="col-sm-12">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon"><img src="/img/settings.png" width="20" alt=""/></span>
                            <input type="text" name="info" class="form-control"
                                   aria-describedby="inputGroupSuccess1Status" placeholder="Special Instructions">
                        </div>
                        <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                                 alt=""/></span>
                    </div>
                </div>

                <div style="clear:both;"></div>
                <hr/>

            </div>
            <div class="col-sm-12">
                <div class="form-group has-success has-feedback">
                    <div class="input-group">
                        <span class="input-group-addon"><img src="/img/tip.png" width="20" alt=""/></span>
                        <input type="number" name="amount_tip" class="form-control" id="input-amount-tip"
                               aria-describedby="inputGroupSuccess1Status" placeholder="Amount Tip">
                    </div>
                    <span class="glyphicon glyphicon-chevron-right form-control-feedback" aria-hidden="true"></span>
                    <span id="inputGroupSuccess1Status" class="sr-only"><img src="/img/arrow.png" width="10"
                                                                             alt=""/></span>
                </div>
            </div>

            <div style="clear:both;"></div>

            <div class="block">
                <div class="col-xs-4">
                    <div class="col-xs-10"><img src="/img/routeicon.png" class="img-responsive" alt=""/></div>
                    <div style="clear:both;"></div>
                    <div class="costedParameters">
                        <span class="miles">0.0</span><br/>Miles
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-10"><img src="/img/timeicon.png" class="img-responsive" alt=""/></div>
                    <div style="clear:both;"></div>
                    <div class="costedParameters">
                        <span class="time">0</span><br/> <font id="unit">Min</font>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-10"><img src="/img/priceicon.png" class="img-responsive" alt=""/></div>
                    <div style="clear:both;"></div>
                    <div class="costedParameters">
                        <span class="sum">0.00</span><br/>Dollars
                    </div>
                </div>
            </div>

        </div>

        <div style="clear:both;"></div>

        <input type="hidden" id="input-travel-time" value="0"/>

        <div class="col-xs-12">
            <a id="m_btn" class="main__get-order-form btn btn-primary inner-btn" href="#">PAY FOR TRIP</a>
        </div>

    </form>
@endsection
