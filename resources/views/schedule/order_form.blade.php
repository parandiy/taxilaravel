{!! Form::open([
    'route' => ['schedule.store'],
    'method' => 'POST',
    'accept-charset' => 'UTF-8',
    'id'=>'orderForm',
]) !!}

<!-- Payment method -->
<div class="payment__method">

    <div id="payment_method_swither" style="width: 280px; margin: 20px auto;">
        <div>
            <div>
                {{Form::radio('payment_method', 'card', true, ['class'=> 'payment-method-toggle toggle-one form-control', 'style'=>'float:right;width:100px;border: 0px solid #ccc;outline: 0px;box-shadow: none;'])}}
            </div>
            <div style="display: inline-block;line-height: 3em;">
                <label style="font-size: 1.5em !important;">Credit Card</label>
            </div>
        </div>
        <div {!! (empty(request()->input('datatime')) ? 'style="display: none;"' : '') !!}>
            <div>
                {{Form::radio('payment_method', 'cash', false, ['class'=> 'payment-method-toggle toggle-one form-control', 'style'=>'float:right;width:100px;border: 0px solid #ccc;outline: 0px;box-shadow: none;'])}}
            </div>
            <div style="display: inline-block;line-height: 3em;">
                <label style="font-size: 1.5em !important;">Cash</label>
            </div>
        </div>
    </div>
    <!--Payment method block -->

    <div id="payment_method_block" class="form_payment_method_block">
        <div class="row">
            <div class="form-group col-xs-9">
                <label for="credit_card_number">CARD NUMBER:</label>
                {{Form::text('credit_card_number', old('credit_card_number'), ['class'=> 'form-control', 'id'=>'credit_card_number', 'placeholder'=> 'XXXXXXXXXXXXXXXX'])}}
            </div>

            <div class="form-group col-xs-3">
                <label for="card_number_cvv">CVV:</label>
                {{Form::text('card_number_cvv', old('card_number_cvv'), ['class'=> 'form-control', 'id'=>'card_number_cvv', 'placeholder'=> 'XXX'])}}
            </div>
        </div>


        <div style="clear:both;"></div>

        <div class="row">
            <div class="form-group col-xs-6">
                <label for="payer_name">Card Holder name:</label>
                {{Form::text('payer_name', old('payer_name'), ['class'=> 'form-control', 'id'=>'payer_name'])}}
            </div>


            <div class="col-xs-6">
                <label for="expiration_month">Expiration:</label>
                <div style="clear:both;"></div>
                <div class="form-group col-xs-6">
                    {{Form::select('expiration_month', array(1 => "01", 2 => "02", 3 => "03", 4 => "04", 5 => "05", 6 => "06", 7 => "07", 8 => "08", 9 => "09", 10 => "10", 11 => "11", 12 => "12"), 1, ['class'=> 'form-control', 'id'=>'expiration_month'])}}
                </div>
                <div class="form-group col-xs-6">
                    @php
                        $year_start = date('y');
                        $year_end = date('y') + 10;
                        $years = array();
                        for($i = $year_start; $i <= $year_end; $i++):
                            $years[$i] = $i;
                        endfor;
                    @endphp
                    {{Form::select('expiration_year', $years, '', ['class'=> 'form-control', 'id'=>'expiration_year'])}}
                </div>
            </div>
        </div>

        <div style="clear:both;"></div>
    </div>
    <div id="payment_cash_block" class="form_payment_method_block" style="display: none">
        <div class="row">
            <div class="form-group col-xs-9">
                <label for="user_name">FIRST AND LAST NAME:</label>
                {{Form::text('user_name', old('user_name'), ['class'=> 'form-control', 'id'=>'user_name', 'placeholder'=> ''])}}
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>

    <!--/Payment method block -->

</div>


<div class="row">
    <div class="form-group col-xs-12">
        <label for="user_address">Address:</label>
        {{Form::text('user_address', old('user_address'), ['class'=> 'form-control', 'id'=>'user_address'])}}
    </div>
</div>

<div class="row">
    <div class="form-group col-xs-5">
        <label for="user_city">City:</label>
        {{Form::text('user_city', old('user_city'), ['class'=> 'form-control', 'id'=>'user_city'])}}
    </div>
    <div class="form-group col-xs-4">
        <label for="state_id">State:</label>
        {{Form::select('state_id', $states, '', ['class'=> 'form-control', 'id'=>'user_city'])}}
    </div>
    <div class="form-group col-xs-3">
        <label for="user_zipcode">Zip:</label>
        {{Form::text('user_zipcode', old('user_zipcode'), ['class'=> 'form-control', 'id'=>'user_zipcode'])}}
    </div>
</div>

<div class="row">
    <div class="form-group col-xs-12">
        <label for="user_email">Email:</label>
        {{Form::text('user_email', old('user_email'), ['class'=> 'form-control', 'id'=>'user_email'])}}
    </div>
</div>

<div class="row">
    <div class="form-group col-xs-12">
        <label for="user_confirm_email">Confirm email:</label>
        {{Form::text('user_confirm_email', old('user_confirm_email'), ['class'=> 'form-control', 'id'=>'user_confirm_email'])}}
    </div>
</div>

<div class="row">
    <div class="form-group col-xs-12">
        <label for="user_phone">Phone:</label>
        {{Form::text('user_phone', old('user_phone'), ['class'=> 'form-control', 'id'=>'user_phone'])}}
    </div>
</div>

<div style="clear:both;"></div>

<div class="checkbox">
    <label>
        {{Form::checkbox('send_notify', 1, true)}} can we text or call you at this number when the cab is on its way?
    </label>
</div>

<div style="clear:both;"></div>
{!! htmlFormSnippet() !!}

<div style="clear:both;"></div>

<div class="checkbox i_agree">
    <label>
        {{Form::checkbox('i_agree')}}
        I agree the trip sales price is non-refundable, credit may be used to apply to future trips
    </label>
</div>

<br/>
{{Form::hidden('amount_payment', request()->input('amount_payment'))}}
{{Form::hidden('amount_tip', request()->input('amount_tip', 0))}}
{{Form::hidden('cab_number', request()->input('cabNumber'))}}
{{Form::hidden('start', request()->input('start'))}}
{{Form::hidden('finish', request()->input('finish'))}}
{{Form::hidden('datatime', request()->input('datatime'))}}
{{Form::hidden('comments', request()->input('comments'))}}
{{Form::hidden('tTime', request()->input('tTime'))}}
{{Form::hidden('distance', request()->input('distance'))}}
{{Form::hidden('passengers', request()->input('passengers', 1))}}
{{Form::hidden('user_timezone_offset', request()->input('user_timezone_offset', 0))}}
{!! Form::close() !!}
