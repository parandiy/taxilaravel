<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="ksut3Y5OqbeOt9f385uxByRpjyZ6qvVPNxCY2ONYBIs"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Taxi</title>

    <link rel="stylesheet" href="/css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="/css/main.css" media="screen">
    <link rel="stylesheet" href="/css/inner.css" media="screen">
    <link rel="stylesheet" href="/css/bootstrap-dialog.css">
    <link rel="stylesheet" href="/css/iosOverlay.css">
    <link rel="stylesheet" href="/css/prettify.css">

    <script type="text/javascript" src="/js/modernizr-2.8.3.min.js"></script>

    @php($settings = \App\Models\Setting::pluck('value', 'key')->toArray())
    <script type="text/javascript">
        var oneml = {{(float)$settings['price_per_mile']}}; // Per one mile
        var passengerFee = {{(float)$settings['passenger_fee']}}; // passenger Fee
        var payPerSeconds = {{(float)$settings['pay_per_seconds']}}; // passenger Fee
        var payPerSecondsSum = {{(float)$settings['pay_per_seconds_sum']}}; // passenger Fee
        var payPerExtraPassengerSum = {{(float)$settings['price_extra_passenger']}}; // passenger Fee
        var flatFee = '1'; // Flat fee
        var travelFee = '1'; // Per travel time
        var poneMile = '1'; // Per one mile
        var baggageFee = '1'; // baggage Fee
    </script>
    {!! htmlScriptTagJsApi() !!}
    <script src="https://www.google.com/recaptcha/api.js?render=explicit"></script>
</head>
<body class="{{(old('remote_pay') == true) ? 'nobg' : ''}}">

<div id="content">
    @yield('content')
</div>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCjyfAk6sMT7fy-q0eJr1HSAETsbmwmcUA&libraries=places"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/js/moment-with-locales.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/js/collapse.js"></script>
<script type="text/javascript" src="/js/transition.js"></script>
<script type="text/javascript" src="/js/bootstrap-dialog.js"></script>
<script type="text/javascript" src="/js/apiGPAutocomp.js?v=1"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/iosOverlay.js"></script>
<script type="text/javascript" src="/js/spin.min.js"></script>
<script type="text/javascript" src="/js/prettify.js"></script>

<script type="text/javascript">
    $(function () {

        $('#datetimepickerp-bootstrap').datetimepicker({
            format: 'dddd MM/DD/YYYY hh:mm A',
            minDate: moment(),
        });
    });
</script>
@yield('footer_scripts')

</body>
</html>
