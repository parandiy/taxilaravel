@extends('layouts.app')

@section('content')
    <div class="top-indent">&nbsp;</div>

    <div class="main" id="pad">
        <div class="logotype"><img src="/img/logotype.png" class="img-responsive" alt=""/></div>
        <div style="clear:both;"></div>
        <div class="col-xs-12" style="color:#FFFFFF;">
            <h1>Privacy Policy</h1>
            <p>
                This privacy policy discloses the privacy practices for www.Associate.Taxi. This privacy policy applies
                solely to information collected by this web site. It will notify you of the following:
            </p>
            <ul>
                <li>What personally identifiable information is collected from you through the web site, how it is used and with whom it may be shared.</li>
                <li>What choices are available to you regarding the use of your data.</li>
                <li>The security procedures in place to protect the misuse of your information.</li>
                <li>How you can correct any inaccuracies in the information.</li>
            </ul>
            <h3>Information Collection, Use, and Sharing</h3>
            <p>We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p>
            <p>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization.</p>
            <p>Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>
            <h3>Your Access to and Control Over Information</h3>
            <p>You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p>
            <ul>
                <li>See what data we have about you, if any.</li>
                <li>Change/correct any data we have about you.</li>
                <li>Have us delete any data we have about you.</li>
                <li>Express any concern you have about our use of your data.</li>
            </ul>
            <h3>Security</h3>
            <p>We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p>
            <p>Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a closed lock icon at the bottom of your web browser or looking for “https” at the beginning of the address of the web page.</p>
            <p>While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>
            <h3>Updates</h3>
            <p>Our Privacy Policy may change from time to time and all updates will be posted on this page.</p>
            <p>If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at (585)436-8750 or via email service@associate.taxi.</p>
        </div>
    </div>

    <div class="footer col-xs-12 col-md-12">
        <div class="phone-bottom">
            <!--img src="/img/phone.png" class="img-responsive" alt=""/-->
            <a href="tel:5852323232"
               style="font-size: 76px; text-decoration: none; color: #FFFFFF; text-shadow: 0 1px 2px rgba(0,0,0,0.5);">585-232-3232</a>
        </div>
        <div>
            Associate Taxi of Rochester, NY Inc.<br/>
            790 S. Plymouth Ave<br/>
            Rochester, New York 14608<br/>
            (585)436-8750 Office/ (585)232-3232 Dispatch<br/>
            <a style="color: #FFFFFF;" href="mailto:service@associate.taxi">service@associate.taxi</a>
        </div>
        <div>
            <iframe width="600" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=new%20york&t=&z=11&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>
        <div style="margin: 10px 0;">
            <a style="color:#ffbe25; margin-right: 10px;" href="{{route('terms')}}">Terms</a>
            <a style="color:#ffbe25;" href="{{route('privacy')}}">Privacy Policy</a>
        </div>
        <div class="text">&copy; 2020, <span> Associate Taxi of Rochester Inc</span></div>
    </div>
@endsection
