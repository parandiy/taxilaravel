<?php

return [
    'license_type' => env('GOOGLE_LICENSE_TYPE', 'standard'),

    'key' => env('GOOGLE_KEY', 'AIzaSyA3rTNT3JDlJ_JnTkKGOBb98gYxSy2jtAU'),

    'client_id'      => env('GOOGLE_CLIENT_ID', ''),
    'encryption_key' => env('GOOGLE_ENCRYPTION_KEY', ''),
];
